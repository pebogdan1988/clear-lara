<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->handleApiExceptions();

        $this->reportable(function (Throwable $e) {
            //
        });
    }

    private function handleApiExceptions(): void
    {
        $this->renderable(function (Throwable $e, $request): bool|JsonResponse {
            if (!$request->is('api/*')) {
                return false;
            }

            return response()->json([
                'message' => $e->getMessage(),
                'code'    => $e->getCode(),
            ], 404);
        });
    }
}
