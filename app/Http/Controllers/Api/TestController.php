<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TestController extends Controller
{
    public function test(Request $request): JsonResponse
    {
        throw new \InvalidArgumentException();
        Cache::put('key', 123);

        return new JsonResponse(Cache::get('key'));
    }
}
