<?php

namespace App\Console\Commands\Checker;

readonly class HttpChecker implements CheckerInterface
{
    public function __construct(public string $url)
    {
    }

    public function check(): CheckResult
    {
        if (!rand(0, 5)) {
            return CheckResult::failed();
        }

        return CheckResult::ok();
    }
}
