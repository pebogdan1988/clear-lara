<?php

namespace App\Console\Commands\Checker;

use Illuminate\Console\Command;

class Check extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run check';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $batchChecker = new BatchHttpChecker();
        $batchChecker->addChecker(new HttpChecker('www.google.com'))
            ->addChecker(new HttpChecker('www.yahoo.com'))
            ->addChecker(new HttpChecker('www.ukr.net'))
            ->addChecker(new HttpChecker('www.MacPaw.ua'));

        $result = $batchChecker->check();

        if (!$result->isOk()) {
            dd(['failed', $batchChecker->getFailed()]);
        }

        dd(['ok', $batchChecker->getResults()]);

        return self::SUCCESS;
    }
}
