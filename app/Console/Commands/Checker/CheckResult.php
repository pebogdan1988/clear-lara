<?php

namespace App\Console\Commands\Checker;

readonly class CheckResult
{
    private const STATUS_OK     = 1;
    private const STATUS_FAILED = 0;

    private function __construct(public int $status)
    {
    }

    public static function ok(): self
    {
        return new self(self::STATUS_OK);
    }

    public static function failed(): self
    {
        return new self(self::STATUS_FAILED);
    }

    public function isOk(): bool
    {
        return $this->status === self::STATUS_OK;
    }
}
