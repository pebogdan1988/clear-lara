<?php

namespace App\Console\Commands\Checker;

interface CheckerInterface
{
    public function check(): CheckResult;
}
