<?php

namespace App\Console\Commands\Checker;

class BatchHttpChecker implements CheckerInterface
{
    private array $httpCheckers = [];
    private array $result       = [];
    private bool  $hasFailed    = false;
    private array $failed       = [];

    public function addChecker(HttpChecker $checker): self
    {
        $this->httpCheckers[] = $checker;

        return $this;
    }

    public function check(): CheckResult
    {
        $this->resetCheckData();

        /** @var HttpChecker $checker */
        foreach ($this->httpCheckers as $checker) {
            $result = $checker->check();

            $this->result[$checker->url] = $result;

            if (!$result->isOk()) {
                $this->failed[$checker->url] = $result;
                $this->hasFailed             = true;
            }
        }

        return !$this->hasFailed
            ? CheckResult::ok()
            : CheckResult::failed();
    }

    public function getFailed(): array
    {
        return $this->failed;
    }

    public function getResults(): array
    {
        return $this->result;
    }

    private function resetCheckData(): void
    {
        $this->result    = [];
        $this->failed    = [];
        $this->hasFailed = false;
    }
}
